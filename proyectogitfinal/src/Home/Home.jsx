import { Banner } from './Banner'
import gitQueEs from '../assets/img/Home/gitQueEs.png'
import linux from '../assets/img/Home/linux.png'
import mac from '../assets/img/Home/mac.jpg'
import windows from '../assets/img/Home/windows.png'
import remoteRepository from '../assets/img/Home/remoteRepository.jpg'
import './Home.css'
export function Home() {

    return (
        <div>
            <Banner />
            <div className='div-content-principal'>
                <div className='div-title-paragraph'>
                    <div className=' div-title ' >
                        <img
                            style={{ height: '50px', width: '100px' }}
                            src={gitQueEs}
                            className='card-img '
                            alt='que-es-git'
                        />
                        <h1 className=' py-5 fw-bolder text-left'> ¿ QUE ES GIT ?</h1>
                    </div>
                    <div className='div-content-1'>
                        <p className='div-text text-justify'>
                            Hoy en día, Git es, con diferencia, el sistema de control de
                            versiones moderno más utilizado del mundo. Git es un proyecto de
                            código abierto maduro y con un mantenimiento activo que desarrolló
                            originalmente Linus Torvalds, el famoso creador del kernel del sistema
                            operativo Linux, en 2005. Un asombroso número de proyectos de software
                            dependen de Git para el control de versiones, incluidos proyectos comerciales
                            y de código abierto. Los desarrolladores que han trabajado con Git cuentan
                            con una buena representación en la base de talentos disponibles para el desarrollo
                            de software, y este sistema funciona a la perfección en una amplia variedad de sistemas
                            operativos e IDE (entornos de desarrollo integrados).
                        </p>

                    </div>
                </div>
                <div className='div-image'>
                    <img
                        style={{ height: '400px', width: '500px' }}
                        src={remoteRepository}
                        className='card-img '
                        alt='ejemploRepositorio'
                    />
                </div>
            </div>
            <div className='div-content-install'>
                <div className='div-title-install'>
                    <h1>INSTALACIÓN DE GIT</h1>
                </div>
                <div className='div-cards'>
                    <div className='div-card'>

                        <div className='div-title-so'>

                            <img
                                style={{ height: '50px', width: '50px' }}
                                src={linux}
                                className='card-img '
                                alt='que-es-git'
                            />
                            <h2> LINUX</h2>
                        </div>
                        <p className='div-paragraph'>
                            Si quieres instalar Git en Linux a través de un instalador binario,
                            en general puedes hacerlo mediante la herramienta básica de administración
                            de paquetes que trae tu distribución. Si estás en Fedora por ejemplo,
                            puedes usar yum:
                        </p>

                        <p className='comands'>$ yum install git</p>
                        <p className='div-paragraph'>Si estás en una distribución basada en Debian como Ubuntu, puedes usar apt-get:</p>
                        <p className='comands'>$ apt-get install git</p>

                    </div>

                    <div className='div-card'>
                        <div className='div-title-so'>

                            <img
                                style={{ height: '50px', width: '50px' }}
                                src={mac}
                                className='card-img '
                                alt='que-es-git'
                            />
                            <h2>MAC</h2>
                        </div>
                        <p className='div-paragraph'>
                            Hay varias maneras de instalar Git en un Mac. Probablemente la más sencilla es instalando
                            las herramientas Xcode de Línea de Comandos. En Mavericks (10.9 o superior) puedes hacer
                            esto desde el Terminal si intentas ejecutar git por primera vez. Si no lo tienes instalado, te
                            preguntará si deseas instalarlo.
                            Si deseas una versión más actualizada, puedes hacerlo a partir de un instalador binario. Un instalador
                            de Git para OSX es mantenido en la página web de Git. Lo puedes descargar
                            en http://git-scm.com/download/mac.
                        </p>
                    </div>

                    <div className='div-card'>
                        <div className='div-title-so'>

                            <img
                                style={{ height: '30px', width: '50px' }}
                                src={windows}
                                className='card-img '
                                alt='que-es-git'
                            />
                            <h2>WINDOWS</h2>
                        </div>
                        <p className='div-paragraph'>
                            También hay varias maneras de instalar Git en Windows. La forma más oficial está disponible para
                            ser descargada en el sitio web de Git. Solo tienes	que	visitar	http://git- scm.com/download/win
                            y la descarga empezará automáticamente. Fíjate que éste es un proyecto conocido como Git para Windows
                            (también llamado msysGit), el cual es diferente de Git. Para más información acerca de este proyecto visita http://msysgit.github.io/.
                        </p>
                    </div>
                </div>
            </div>

        </div>
    );
}