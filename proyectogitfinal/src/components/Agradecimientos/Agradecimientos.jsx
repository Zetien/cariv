import bannerImg from '../../assets/img/banner.png';
import { CardAgradecimiento } from './CardAgradecimiento.jsx';

export function Agradecimientos() {
  return (
    <div>
      <div className='card mb-3'>
        <img
          style={{ height: '150px', objectFit: 'cover' }}
          src={bannerImg}
          className='card-img-top'
          alt='banner'
        />
        <div className='card-body'>
          <h1 className='card-title text-center fs-1'>
            ¡Muchas gracias Profesor Fredy!
          </h1>
        </div>
      </div>
      <div className='container'>
        <div className='row'>
          <CardAgradecimiento
            nombre={'María Anaya'}
            descripcion={
              'Con este proyecto pudimos integrar no solo lo aprendido en GIT sino en SCRUM y REACT. valorar a quien comparte conocimiento, es un regalo maravilloso ¡ muchas gracias !'
            }
          />
          <CardAgradecimiento
            nombre={'Erick Contreras Barrios'}
            descripcion={
              'Muchas Gracias Profe fredy, fue muy duro ser PM pero aprendi demasiado, espero vernos pronto en nokia, fue muy chevere el tiempo compartido juntos.....¡ muchas gracias !'
            }
          />
          <CardAgradecimiento
            nombre={'Zuleydis Barrios Peña'}
            descripcion={
              'Profe Fredy, muchas gracias por el gran apoyo, gracias por esas clases de gitlab y me pareció super este proyecto pues con esto consolidamos lo aprendido en todas las demas clases.'
            }
          />
          <CardAgradecimiento
            nombre={'Jorge Luis Zetien Luna'}
            descripcion={
              'Profesor muchas gracias por dictarnos su conocimiento adquirido y sus experiencias en sobre el git, Aprendi bastante ya que es vital para futuros proyectos'
            }
          />
          <CardAgradecimiento
            nombre={'Leandro Meza vasquez '}
            descripcion={
              'Gracias Profe Fredi por tu paciencia y tu entrega a la hora de enseñarnos, por ponernos retos que nos hacen crecer como personas y profesionales.'
            }
          />
          <CardAgradecimiento
            nombre={'Amanda Marcela Quintana Julio'}
            descripcion={
              'Fue un excelente curso. Tuve la oportunidad de aprender muchos de los comandos de Git y ponerlos en practica en muchos proyectos. Muchas gracias!!!'
            }
          />
          <CardAgradecimiento
            nombre={'Ronald Paternina Castro'}
            descripcion={
              'Muchas gracias por compartir sus conocimientos de git y gitlab con nosotros profe (￣▽￣)//, ojalá en un futuro pueda impartirnos más cursos o compartir sus experiencias profesionales.'
            }
          />
          <CardAgradecimiento
            nombre={'Angélica Morales Daza'}
            descripcion={
              'Fue un excelente donde se aprendio acerca de los comandos y buenas prácticas en gitlab, gracias por todo el conocimiento impartido :)'
            }
          />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
          <CardAgradecimiento nombre={'nombre'} descripcion={'descripcion'} />
        </div>
      </div>
    </div>
  );
}
